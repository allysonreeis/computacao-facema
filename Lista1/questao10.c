/*Aluno: Allyson Reis Vieira de Aguiar 16133686
* Lista 01 --- Facema - Computa��o II
* Meu reposit�rio: https://gitlab.com/allysonreis1/computacao-facema
*/

#include<stdio.h> //Biblioteca de entrada e sa�da de dados
#define TAM 7 //Constante de pr�-processamento #define. Constante simb�lica


main () {
	int valores[TAM];
	int i, soma;
	
	puts("Digite 7 valores inteiros");
	for (i = 0, soma = 0; i < TAM; i++) {
		printf("Valor %d: ", i+1);
		scanf("%d", &valores[i]);
		
		soma = soma + valores[i];
	}
	
	printf("A soma dos valores inseridos e: %2d\n", soma);
}
