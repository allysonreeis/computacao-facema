/*Aluno: Allyson Reis Vieira de Aguiar 16133686
* Lista 01 --- Facema - Computa��o II
* Meu reposit�rio: https://gitlab.com/allysonreis1/computacao-facema
*/

#include<stdio.h> //Biblioteca de entrada e sa�da de dados

main () {
	int numero;
	
	printf("Insira um numero: ");
	scanf("%d", &numero);
	
	//Resto da Divis�o inteira. Se o resto for 0, implica que, o n�mero digitado � par
	if (numero%2 == 0)
		printf("%d e par\n", numero);
	else 
		printf("%d e impar", numero);
}
