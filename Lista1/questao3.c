/*Aluno: Allyson Reis Vieira de Aguiar 16133686
* Lista 01 --- Facema - Computa��o II
* Meu reposit�rio: https://gitlab.com/allysonreis1/computacao-facema
*/

#include<stdio.h> //Biblioteca de entrada e sa�da de dados
#include<string.h> //Biblioteca para uso da fun��o strlen()
#define TAM 20 //Constante simb�lica

main () {
	char nome[TAM];
	float nota1, nota2, nota3, mediaFinal;
	int str_size;
	
	printf("Insira o nome do aluno: ");
	fgets(nome, TAM, stdin); //A fun��o fgets � a melhor escolha para leitura de string. Ela impede a leitura de mais caracteres do que o permitido
	
	fflush(stdin);//limpa o buffer do teclado
	
	printf("Insira a 1.o nota: ");
	scanf("%f", &nota1);
	
	printf("Insira a 2.o nota: ");
	scanf("%f", &nota2);
	
	printf("Insira a 3.o nota: ");
	scanf("%f", &nota3);
	
	putchar('\n');
	
	mediaFinal = (nota1 + nota2 + nota3)/3;
	
	//A fun��o strlen() verifica quantos caracteres foram inseridos e retorna um valor inteiro
	str_size = strlen(nome);
	
	//Removendo o '\n' e colocando o caractere delimitador '\0'
	if (nome[str_size-1] == '\n')
		nome[str_size-1] = '\0';
		
	printf("Aluno: %s\nMedia Final: %.2f\n\n", nome, mediaFinal);
}
