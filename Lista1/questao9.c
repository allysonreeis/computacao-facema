/*Aluno: Allyson Reis Vieira de Aguiar 16133686
* Lista 01 --- Facema - Computa��o II
* Meu reposit�rio: https://gitlab.com/allysonreis1/computacao-facema
*/

#include<stdio.h> //Biblioteca de entrada e sa�da de dados
#define TAM 20 //Constante simb�lica
#include<string.h>

main () {
	char primeiroNome[TAM], segundoNome[TAM], terceiroNome[TAM];
	int size_str1, size_str2, size_str3;
		
	printf("Digite o primeiro nome: ");
	fgets(primeiroNome, TAM, stdin);
	
	fflush(stdin);
	
	printf("Digite o segundo nome: ");
	fgets(segundoNome, TAM, stdin);
	
	fflush(stdin);
	
	printf("Digite o terceiro nome: ");
	fgets(terceiroNome, TAM, stdin);
	
	fflush(stdin);
	
	//a fun��o strlen() verifica quantos caracteres foram escritos na palavra 
	size_str1 = strlen(primeiroNome);
	size_str2 = strlen(segundoNome);
	size_str3 = strlen(terceiroNome);
	
	//Removendo '\n' e colocando o caractere delimitador '\0'
	if (primeiroNome[size_str1 - 1] == '\n')
		primeiroNome[size_str1 - 1] = '\0';
		
	if (segundoNome[size_str2 - 1] == '\n')
		segundoNome[size_str2 - 1] = '\0';
		
	if (terceiroNome[size_str3 - 1] == '\n')
		terceiroNome[size_str3 - 1] = '\0';
	
	printf("O nome do usuario e: %s %s %s\n", primeiroNome, segundoNome, terceiroNome);
}
